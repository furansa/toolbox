#!/usr/bin/env bash
#
# Index music albums to a HTML file. Usage, from command line or crontab:
#
# $ ./index_music_albums.sh
#
# References:
#
# http://www.unix.com/shell-programming-and-scripting/149817-cut-command-delimiter-reverse.html
# http://www.gnu.org/software/bash/manual/bashref.html#Shell-Parameter-Expansion
#
ALBUMS="${HOME}/Music/Albums"
FILE="${ALBUMS}/index.html"
TOC="${ALBUMS}/toc.html"
NEWEST="${ALBUMS}/Newest.m3u"

echo "Finding"

find ${ALBUMS} -type d -exec chmod 0755 {} \;
find ${ALBUMS} -type f -name \*.flac -exec chmod 0444 {} \;
find ${ALBUMS} -type f -name \*.mp3 -exec chmod 0444 {} \;
find ${ALBUMS} -type f -name \*.jpg -exec chmod 0444 {} \;
find ${ALBUMS} -type f -name \*.m3u -exec chmod 0444 {} \;

tree --prune -d -L 1 --noreport -H ${ALBUMS} --houtro=/dev/null -C -T "Music Albums" ${ALBUMS} -o ${TOC}
tree --prune -d -L 3 -I "0 - Organize" -H ${ALBUMS} --hintro=${TOC} -C -T "Music Albums" ${ALBUMS} -o ${FILE}
rm -f ${TOC}

# Create a playlist with the newest added files
rm -f ${NEWEST}
find ${ALBUMS} -type f -newermt $(date +%Y-%m-%d -d '30 days ago') | egrep -v 'History|index|jpg|m3u|Newest|Organize' | sort > ${NEWEST}

# Custom playlists
chmod 0660 ${ALBUMS}/*.m3u

echo "Done"

