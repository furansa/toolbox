#!/bin/bash

url_m2_analog="http://88.190.31.35:9080";
url_m2_chillout="http://91.121.99.205:9000";
url_m2_digital="http://88.190.31.35:8090";
url_m2_rock="http://91.121.99.205:8050";
date=$(date +%Y%m%d);

avconv -i ${url_m2_analog} -t 3600:00 -acodec libmp3lame "M2 Analog - ${date}.mp3";
sleep 10;
avconv -i ${url_m2_chillout} -t 3600:00 -acodec libmp3lame "M2 Chillout - ${date}.mp3";
sleep 10;
avconv -i ${url_m2_digital} -t 3600:00 -acodec libmp3lame "M2 Digital - ${date}.mp3";
sleep 10;
avconv -i ${url_m2_rock} -t 3600:00 -acodec libmp3lame "M2 Rock - ${date}.mp3";
