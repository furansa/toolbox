#!/usr/bin/env bash
#
# Index book files to a HTML file. Usage, from command line or crontab:
#
# $ ./index_bookshelf.sh
#
# Index EPUB and PDF files to a HTML file. Usage, from command line or crontab:
#
# $ ./index_bookshelf.sh

# References:
#
# http://www.unix.com/shell-programming-and-scripting/149817-cut-command-delimiter-reverse.html
# http://www.gnu.org/software/bash/manual/bashref.html#Shell-Parameter-Expansion
#
BOOKS="${HOME}/Documents/Bookshelf"
FILE="${BOOKS}/index.html"
TOC="${BOOKS}/toc.html"

echo "Finding"

find ${BOOKS} -type d -exec chmod 0755 {} \;
find ${BOOKS} -type f -name \*.epub -exec chmod 0444 {} \;
find ${BOOKS} -type f -name \*.pdf -exec chmod 0444 {} \;

tree --prune -d -L 1 --noreport -H ${BOOKS} --houtro=/dev/null -C -T "Bookshelf" ${BOOKS} -o ${TOC}
tree --prune -P *.epub -P *.pdf -I "0 - Organize" -H ${BOOKS} --hintro=${TOC} -C -T "Bookshelf" ${BOOKS} -o ${FILE}
rm -f ${TOC}

echo "Done"

