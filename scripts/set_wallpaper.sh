#!/usr/bin/env bash
#
# Set wallpaper according the period
#
# References:
#
# https://wiki.archlinux.org/title/Redshift
# https://rafaelc.org/posts/automatically-switch-to-dark-theme-with-redshift-hooks
#
set_wallpaper() {
    local period=${1}
    local wallpaper="${HOME}/Pictures/Wallpapers/Wallpaper.png"

    case ${period} in
        "day")
            ln -sf "${HOME}/Pictures/Wallpapers/Material Sunset 4480x1080.png" "${wallpaper}"
            ;;
        "night")
            ln -sf "${HOME}/Pictures/Wallpapers/Material Sunset 4480x1080 Grayscale.png" "${wallpaper}"
            ;;
    esac

    xsetbg -display :0 -gamma 2 "${wallpaper}" 2>> /dev/null
    ps x -o pid,command | grep scratchterm | awk '{print $1}' | xargs kill 2> /dev/null
    urxvt -name scratchterm -e bash &
}

hour=$(date +%H)

if [ $hour -ge 7 ] && [ $hour -lt 19  ]; then
    set_wallpaper "day"
elif [ $hour -ge 19 ] || [ $hour -lt 7  ]; then
    set_wallpaper "night"
fi

