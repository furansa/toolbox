#!/usr/bin/env bash
#
# Custom i3status bar script
#
# References:
#
# https://github.com/i3/i3/blob/next/contrib/trivial-bar-script.sh
# https://en.jeffprod.com/blog/2020/create-your-own-i3-sway-status-bar
# https://forkaweso.me/Fork-Awesome/icons/
# https://draculatheme.com/contribute
#
COLOR_DARK_BLUE="#44475A"
COLOR_DARK_GREY="#282A36"
COLOR_DARK_YELLOW="#A39362"
COLOR_LIGHT_BLUE="#6272A4"
COLOR_LIGHT_CYAN="#8BE9FD"
COLOR_LIGHT_GREEN="#50FA7B"
COLOR_LIGHT_ORANGE="#FFB86C"
COLOR_LIGHT_PINK="#FF79C6"
COLOR_LIGHT_RED="#FF5555"
COLOR_LIGHT_PURPLE="#BD93F9"
COLOR_LIGHT_YELLOW="#F1FA8C"
COLOR_WHITE="#F8F8F2"
COLOR_GOOD="${COLOR_LIGHT_GREEN}"
COLOR_DEGRADED="${COLOR_LIGHT_YELLOW}"
COLOR_BAD="${COLOR_LIGHT_RED}"

alert_memory() {
    local VALUE=${1}
    local LIMIT=70

    [ $(echo "${VALUE}" "${LIMIT}"  | awk '{print ($1 > $2)}') ] ||
        XDG_RUNTIME_DIR=/run/user/$(id -u) notify-send -i important -u critical "Memory consumption too high ${VALUE}"
}

bluetooth_and_network_handler() {
    urxvt -name i3status -e bash -c 'echo -e "Bluetooth connected devices:\n"; bluetoothctl devices Connected; echo -e "\nNetworking:\n"; ip -4 addr show | grep inet; echo -e "\nFirewall:\n"; su -c "/usr/sbin/ufw status"; read -n 1 -r -s' &
}

brightness_and_volume_handler() {
    urxvt -name i3status -e pulsemixer &
}

calendar_handler() {
    urxvt -name i3status -e bash -c 'date +"Lisboa:    %R %a %d/%m/%Y (%j)"; TZ=America/New_York date +"New York:  %R %a %d/%m/%Y"; TZ=America/Sao_Paulo date +"São Paulo: %R %a %d/%m/%Y"; TZ=Australia/Melbourne date +"Melbourne: %R %a %d/%m/%Y"; TZ=Asia/Kolkata date +"Kolkata:   %R %a %d/%m/%Y"; echo -e ""; ncal -b3; echo -e ""; calcurse -a -d 2 --output-datefmt %d/%m/%Y; read -n 1 -r -s' &
}

cpu_and_memory_handler() {
    urxvt -name i3status -e bash -c 'top -b -n 1 | head -n 17; echo ""; acpi -ti; read -n 1 -r -s' &
}

energy_handler() {
    urxvt -name i3status -e bash -c 'acpi -abi; read -n 1 -r -s' &
}

finance_handler() {
    urxvt -name i3status -e bash -c 'finance_scrapper.sh --all; read -n 1 -r -s' &
}

load_handler() {
    urxvt -name i3status -e top &
}

music_handler() {
    urxvt -name i3status -e mocp &
}

show_bluetooth_and_network() {
    local name="show_bluetooth_and_network"
    local bluetooth="\uf294"
    local ethernet="\uf074 $(ip -4 addr show enp0s31f6 | grep inet | awk '{print $2}' | cut -d '/' -f 1)"
    local vpn_ppp="\uf023 $(ip -4 addr show ppp0 | grep inet | awk '{print $2}')"
    local vpn_tun="\uf023 $(ip -4 addr show tun0 | grep inet | awk '{print $2}' | cut -d "/" -f 1)"
    local wireless_link=$(/usr/sbin/iwconfig wlp0s20f3 | grep -w "Link Quality" | cut -d '/' -f 1 | cut -d '=' -f 2)
    local wireless_quality=$(expr ${wireless_link} \* 100 / 70)
    local wireless="\uf1eb $(ip -4 addr show wlp0s20f3 | grep inet | awk '{print $2}' | cut -d '/' -f 1) ($(/usr/sbin/iwconfig wlp0s20f3 | grep ESSID | cut -d ':' -f 2 | cut -d '"' -f 2), ${wireless_quality}%)"
    local content

    [[ $(ip -4 addr show enp0s31f6 | grep "state UP") ]] && content="${content} ${bluetooth} ${ethernet}"
    [[ $(ip -4 addr show ppp0 | grep "state UP") ]] && content="${content} ${bluetooth} ${vpn_ppp}"
    [[ $(ip -4 addr show tun | grep "state UP") ]] && content="${content} ${bluetooth} ${vpn_tun}"
    [[ $(ip -4 addr show wlp0s20f3 | grep "state UP") ]] && content="${content} ${bluetooth} ${wireless}"

    show_empty_separator
    echo -n ",{"
    show_content "${COLOR_LIGHT_PURPLE}" "${name}" "${content}"
    echo -n "},"
}

show_brightness_and_volume() {
    local NAME="brightness_and_volume"
    local LAPTOP_BRIGHTNESS="\uf109 $(xrandr --verbose | egrep 'Brightness' | head -n 1 | cut -d "." -f 2)%"
    local MONITOR_BRIGHTNESS="\uf108 $(xrandr --verbose | egrep 'Brightness' | head -n 2 | tail -n 1 | cut -d "." -f 2)%"
    local VOLUME="\uf028 $(pulsemixer --get-volume | awk '{print $1}')%"

    [[ $(xrandr --verbose | grep HDMI1 | grep -w connected) ]] &&
        local CONTENT="${LAPTOP_BRIGHTNESS} ${MONITOR_BRIGHTNESS} ${VOLUME}" ||
        local CONTENT="${LAPTOP_BRIGHTNESS} ${VOLUME}"

    show_empty_separator
    echo -n ",{"
    show_content "${COLOR_WHITE}" "${NAME}" "${CONTENT}"
    echo -n "},"
}

show_calendar_and_keyboard() {
    local NAME="calendar_and_keyboard"
    local CONTENT="\uf11c $(setxkbmap -query | grep layout | awk '{print $2}') \uf017 $(date +"%R") \uf073 $(date +"%d/%m/%Y")"

    show_empty_separator
    echo -n ",{"
    show_content "${COLOR_WHITE}" "${NAME}" "${CONTENT} "
    echo -n "}"
}

show_content() {
    local FG_COLOR=${1}
    local NAME=${2}
    local CONTENT=${3}

    echo -n "\"background\":\"${COLOR_DARK_GREY}\","
    echo -n "\"color\":\"${FG_COLOR}\","
    echo -n "\"full_text\":\"${CONTENT}\","
    echo -n "\"name\":\"${NAME}\","
    echo -n "\"border\":\"${COLOR_DARK_GREY}\","
    echo -n "\"border_bottom\":2,"
    echo -n "\"border_left\":0,"
    echo -n "\"border_right\":0,"
    echo -n "\"border_top\":2,"
    echo -n "\"separator\":false,"
    echo -n "\"separator_block_width\":0"
}

show_cpu_and_memory() {
    local NAME="cpu_and_memory"
    local CPU="\uf0e7 $(top -b -n 1 | head -n 3 | tail -n 1 | awk '{print $3}' | cut -d '[' -f 1)% (\uf2c9 $(acpi -t | awk '{print $4}')°C)"
    local MEMORY="\uf2db $(free -m | grep Mem | awk '{print ($3 / $2) * 100}')%"
    local CONTENT="${CPU} ${MEMORY}"

    show_empty_separator
    echo -n ",{"
    show_content "${COLOR_LIGHT_PURPLE}" "${NAME}" "${CONTENT}"
    echo -n "},"

    alert_memory $(free -m | grep Mem | awk '{print ($3 / $2) * 100}')
}

show_empty_separator() {
    echo -n "{"
    echo -n "\"full_text\":\"    \","
    echo -n "\"separator\":false,"
    echo -n "\"separator_block_width\":0"
    echo -n "}"
}

show_energy() {
    local NAME="energy"
    local COLOR="${COLOR_LIGHT_PURPLE}"
    local STATUS=$(acpi -a | awk '{print $3}')
    local BATTERY=""

    if [[ ${STATUS} == "on-line" ]]; then
        local CONTENT="\uf1e6 $(acpi -a | awk '{print $3}') ($(acpi -b | egrep -iw 'Charging|Full' | cut -d " " -f 3,4 | cut -d "," -f 1,2))"
        # local CONTENT="\uf1e6 $(acpi -a | awk '{print $3}')"
    else
        local CONTENT="\uf242 $(acpi -b | grep "Battery 0" | awk '{print $4}' | cut -d "," -f 1) ($(acpi -b | grep "Battery 0" | awk '{print $5}'))"

        BATTERY=$(acpi -b | grep "Battery 0" | awk '{print $4}' | cut -d '%' -f 1)
        if (( ${BATTERY} <= 15 )); then
            COLOR=${COLOR_LIGHT_RED}
            notify-send -u critical "Battery is too low"
        fi
    fi

    show_empty_separator
    echo -n ",{"
    show_content "${COLOR}" "${NAME}" "${CONTENT}"
    echo -n "},"
}

show_finance() {
    local NAME="finance"
    local USER_AGENT='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36'
    local BTC_BRL_CONTENT=$(curl -s -H "${USER_AGENT}" https://www.google.com/finance/quote/BTC-BRL)
    local BTC_BRL_QUOTE=$(echo ${BTC_BRL_CONTENT} | egrep -o '<div class="YMlKec fxKbKc">[0-9]+,[0-9]+.[0-9]+</div>' | sed -e 's/<[^>]*>//g')
    local CONTENT="\uf15a ${BTC_BRL_QUOTE}"

    show_empty_separator
    echo -n ",{"
    show_content "${COLOR_WHITE}" "${NAME}" "${CONTENT}"
    echo -n "},"
}

show_load() {
    local NAME="load"
    local CONTENT="\uf0e4 $(uptime | cut -d "," -f 3- | cut -d ":" -f 2-)"

    show_empty_separator
    echo -n ",{"
    show_content "${COLOR_WHITE}" "${NAME}" "${CONTENT}"
    echo -n "},"
}

show_music() {
    local NAME="music"
    local STATUS=$(mocp -Q %state)

    if [[ ${STATUS} == "PAUSE" ]]; then
        local CONTENT="\uf04c $(mocp -Q '%artist - %song')"
    elif [[ ${STATUS} == "PLAY" ]]; then
        local CONTENT="\uf04b $(mocp -Q '%artist - %song')"
    fi

    show_empty_separator
    echo -n ",{"
    show_content "${COLOR_LIGHT_PURPLE}" "${NAME}" "${CONTENT}"
    echo -n "},"
}

show_uptime() {
    local NAME="uptime"
    local CONTENT="\uf2ff $(hostname) $(uptime | cut -d " " -f 3- | cut -d "," -f 1)"

    show_empty_separator
    echo -n ",{"
    show_content "${COLOR_LIGHT_PURPLE}" "${NAME}" "${CONTENT}"
    echo -n "},"
}

show_separator() {
    local BG_COLOR="${1}"
    local FG_COLOR=${2}

    echo -n "{"
    echo -n "\"background\":\"${BG_COLOR}\","
    echo -n "\"border\":\"${COLOR_DARK_GREY}\","
    echo -n "\"border_bottom\":2,"
    echo -n "\"border_left\":0,"
    echo -n "\"border_right\":0,"
    echo -n "\"border_top\":2,"
    echo -n "\"color\":\"${FG_COLOR}\","
    echo -n "\"full_text\":\"\uf053\","
    echo -n "\"separator\":false,"
    echo -n "\"separator_block_width\":0"
    echo -n "}"
}

show_storage() {
    local NAME="storage"
    local COLOR="${COLOR_WHITE}"
    local BACKUP_USAGE="\uf1da$(df -h --output=pcent /media/deathengine/backup | tail -n 1)"
    local HOME_USAGE="$(df -h --output=pcent /home | tail -n 1 | awk '{ print $1 }' | cut -d '%' -f 1)"
    local HOME_USAGE_PCENT="\uf015$(df -h --output=pcent /home | tail -n 1)"
    local ROOT_USAGE="$(df -h --output=pcent / | tail -n 1 | awk '{ print $1 }' | cut -d '%' -f 1)"
    local ROOT_USAGE_PCENT="\uf0a0$(df -h --output=pcent / | tail -n 1)"
    local STORAGE_USAGE="\uf114$(df -h --output=pcent /media/deathengine/storage | tail -n 1)"
    local CONTENT="${ROOT_USAGE_PCENT} ${HOME_USAGE_PCENT}"

    [[ $(mount | grep /media/deathengine/backup) ]] &&
        CONTENT="${CONTENT} ${BACKUP_USAGE}"

    [[ $(mount | grep /media/deathengine/storage) ]] &&
        CONTENT="${CONTENT} ${STORAGE_USAGE}"

    if (( ${ROOT_USAGE} >= 90 )); then
        COLOR=${COLOR_LIGHT_RED}
        notify-send -u critical "Root disk space is too low"
    fi

    show_empty_separator
    echo -n ",{"
    show_content "${COLOR}" "${NAME}" "${CONTENT}"
    echo -n "},"
}

storage_handler() {
    urxvt -name i3status -e bash -c 'df -h; read -n 1 -r -s' &
}

uptime_handler() {
    urxvt -name i3status -e bash -c 'uptime; users; echo ""; apt-get --just-print upgrade; read -n 1 -r -s' &
}

echo "{ \"version\": 1, \"click_events\": true }" # Tells i3bar to use JSON
echo "["                                          # Begin the endless array
echo "[]"                                         # Send an empty first array of blocks to make the loop simpler

# Display information
(while :;
do
    echo -n ",["
    show_music
    show_finance
    show_uptime
    show_load
    show_cpu_and_memory
    show_storage
    show_bluetooth_and_network
    show_brightness_and_volume
    show_energy
    show_calendar_and_keyboard
    echo -n "]"
    sleep 5
done) &

# Handle click events
while read line;
do
    if [[ $line == *"name"*"brightness_and_volume"* ]]; then
        brightness_and_volume_handler
    elif [[ $line == *"name"*"calendar_and_keyboard"* ]]; then
        calendar_handler
    elif [[ $line == *"name"*"cpu_and_memory"* ]]; then
        cpu_and_memory_handler
    elif [[ $line == *"name"*"energy"* ]]; then
        energy_handler
    elif [[ $line == *"name"*"finance"* ]]; then
        finance_handler
    elif [[ $line == *"name"*"load"* ]]; then
        load_handler
    elif [[ $line == *"name"*"music"* ]]; then
        music_handler
    elif [[ $line == *"name"*"bluetooth_and_network"* ]]; then
        bluetooth_and_network_handler
    elif [[ $line == *"name"*"storage"* ]]; then
        storage_handler
    elif [[ $line == *"name"*"uptime"* ]]; then
        uptime_handler
    fi
done
