#!/usr/bin/env bash
#
# Download web calendars to update local calcurse. Usage, from command line or
# crontab:
#
# $ ./update_calendars.sh
#
# TODO: Read calendars from a JSON file such as {["name": "url",]}
#
CAL_WEATHER="http://ical.meteomatics.com/calendar/Coimbra/40.203315_-8.410257/en/meteomat.ics"

ICS_WEATHER="/tmp/weather.ics"

wget -q "${CAL_WEATHER}"      -O ${ICS_WEATHER}

# Since not all calendars are very important, only some needs to exist before
# update calcurse
[ -s ${ICS_WEATHER} ] && {
        calcurse -q -P --filter-type cal &&
        calcurse -q -i ${ICS_WEATHER}
    }
