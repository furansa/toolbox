#!/usr/bin/env bash
#
# Perform incremental backup. Usage, from command line:
#
# $ ./incremental_backup.sh /mountpoint /home/myuser /mountpoint/destination /path/to/exclude_list
#
# Or crontab:
#
# 0 0 * * * /path/to/incremental_backup.sh /mountpoint /home/myuser /mountpoint/destination /path/to/exclude_list
#
MOUNT="${1}"
SOURCE="${2}"
DESTINATION="${3}"
EXCLUDE_LIST="${4}"
LOGFILE="${DESTINATION}/$(date +%Y%m%d).log"
ERRFILE="${DESTINATION}/$(date +%Y%m%d).err"

[[ $(mount | grep ${MOUNT}) ]] || mount ${MOUNT} || exit 1

rsync -azvpL --omit-link-times --no-devices --no-specials --exclude-from="${EXCLUDE_LIST}" ${HOME} ${DESTINATION} >> ${LOGFILE} 2>> ${ERRFILE}

[[ -s ${ERRFILE} ]] && XDG_RUNTIME_DIR=/run/user/$(id -u) notify-send -i important -u critical "Backup finished with errors" && exit 0

XDG_RUNTIME_DIR=/run/user/$(id -u) notify-send -i info -u critical "Backup finished"
