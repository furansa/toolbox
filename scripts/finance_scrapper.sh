#!/usr/bin/env bash
#
# Basic finance scrapper
#
ARGV=${1}

USER_AGENT='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36'

function bitcoin {
    BTC_BRL_URL="https://www.google.com/finance/quote/BTC-BRL"
    BTC_BRL_CONTENT=$(curl -s -H "${USER_AGENT}" ${BTC_BRL_URL})
    BTC_BRL_QUOTE=$(echo ${BTC_BRL_CONTENT} | egrep -o '<div class="YMlKec fxKbKc">[0-9]+,[0-9]+.[0-9]+</div>' | sed -e 's/<[^>]*>//g')

    BTC_EUR_URL="https://www.google.com/finance/quote/BTC-EUR"
    BTC_EUR_CONTENT=$(curl -s -H "${USER_AGENT}" ${BTC_EUR_URL})
    BTC_EUR_QUOTE=$(echo ${BTC_EUR_CONTENT} | egrep -o '<div class="YMlKec fxKbKc">[0-9]+,[0-9]+.[0-9]+</div>' | sed -e 's/<[^>]*>//g')

    echo -e "\nBitcoin"
    echo -e "   BTC/BRL: ${BTC_BRL_QUOTE} \n   ${BTC_BRL_URL}\n"
    echo -e "   BTC/EUR: ${BTC_EUR_QUOTE} \n   ${BTC_EUR_URL}\n"
}

function currency {
    EUR_BRL_URL="https://www.google.com/finance/quote/EUR-BRL"
    EUR_BRL_CONTENT=$(curl -s -H "${USER_AGENT}" ${EUR_BRL_URL})
    EUR_BRL_QUOTE=$(echo ${EUR_BRL_CONTENT} | egrep -o '<div class="YMlKec fxKbKc">[0-9]+.[0-9]+</div>' | sed -e 's/<[^>]*>//g')

    echo -e "\nCurrency"
    echo -e "   EUR/BRL: ${EUR_BRL_QUOTE} \n   ${EUR_BRL_URL}\n"
}

function funds {
    BNP_PARIBAS_EQUITY_TURNEY_N_EUR_URL="https://www.deco.proteste.pt/investe/investimentos/fundos/bnpp-turkey-equity-n-cap"
    BNP_PARIBAS_EQUITY_TURNEY_N_EUR_CONTENT=$(curl -s -H "${USER_AGENT}" ${BNP_PARIBAS_EQUITY_TURNEY_N_EUR_URL})
    BNP_PARIBAS_EQUITY_TURNEY_N_EUR_QUOTE=$(echo ${BNP_PARIBAS_EQUITY_TURNEY_N_EUR_CONTENT} | egrep -o '<span class="product-points-data__current-value">[0-9]+,[0-9]+&#160;EUR</span>' | sed -e 's/&#160;EUR//g' | sed -e 's/<[^>]*>//g')

    PICTET_DIGITAL_R_EUR_URL="https://www.deco.proteste.pt/investe/investimentos/fundos/pictet-digital-r-eur"
    PICTET_DIGITAL_R_EUR_CONTENT=$(curl -s -H "${USER_AGENT}" ${PICTET_DIGITAL_R_EUR_URL})
    PICTET_DIGITAL_R_EUR_QUOTE=$(echo ${PICTET_DIGITAL_R_EUR_CONTENT} | egrep -o '<span class="product-points-data__current-value">[0-9]+,[0-9]+&#160;EUR</span>' | sed -e 's/&#160;EUR//g' | sed -e 's/<[^>]*>//g')

    echo -e "\nFunds"
    echo -e "   BNP Paribas Equity Turney N EUR: ${BNP_PARIBAS_EQUITY_TURNEY_N_EUR_QUOTE} \n   ${BNP_PARIBAS_EQUITY_TURNEY_N_EUR_URL}\n"
    echo -e "   Pictet Digital R EUR: ${PICTET_DIGITAL_R_EUR_QUOTE} \n   ${PICTET_DIGITAL_R_EUR_URL}\n"
}

function stocks {
    CPFE3_URL="https://www.google.com/finance/quote/CPFE3:BVMF"
    CPFE3_CONTENT=$(curl -s -H "${USER_AGENT}" ${CPFE3_URL})
    CPFE3_QUOTE=$(echo ${CPFE3_CONTENT} | egrep -o '<div class="YMlKec fxKbKc">..[0-9]+.[0-9]+</div>' | sed -e 's/R$//g' | sed -e 's/<[^>]*>//g')

    GGBR4_URL="https://www.google.com/finance/quote/GGBR4:BVMF"
    GGBR4_CONTENT=$(curl -s -H "${USER_AGENT}" ${GGBR4_URL})
    GGBR4_QUOTE=$(echo ${GGBR4_CONTENT} | egrep -o '<div class="YMlKec fxKbKc">..[0-9]+.[0-9]+</div>' | sed -e 's/R$//g' | sed -e 's/<[^>]*>//g')

    VALE3_URL="https://www.google.com/finance/quote/VALE3:BVMF"
    VALE3_CONTENT=$(curl -s -H "${USER_AGENT}" ${VALE3_URL})
    VALE3_QUOTE=$(echo ${VALE3_CONTENT} | egrep -o '<div class="YMlKec fxKbKc">..[0-9]+.[0-9]+</div>' | sed -e 's/R$//g' | sed -e 's/<[^>]*>//g')

    echo -e "\nStocks"
    echo -e "   CPFE3: ${CPFE3_QUOTE} \n   ${CPFE3_URL}\n"
    echo -e "   GGBR4: ${GGBR4_QUOTE} \n   ${GGBR4_URL}\n"
    echo -e "   VALE3: ${VALE3_QUOTE} \n   ${VALE3_URL}\n"
}

function show_all {
    echo -e "$(date)"
    bitcoin
    funds
    currency
    stocks
}

if [ "${ARGV}" = "--all" ]; then
    show_all
else
    echo "Usage: ./finance_scrapper.sh [--all]"
    exit 0
fi
