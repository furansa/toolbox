#!/usr/bin/env bash
#
# Show notification and save history on song change. Must be called from the
# OnSongChange paramenter at the moc config file:
#
# OnSongChange = "${HOME}/.moc/moc_handle_song_change.sh %a %n %t %f"
#
timestamp=$(/usr/bin/date +%F)
artist="${1}"
track_number="${2}"
track_name="${3}"
file_name="${4}"

dir_name=$(/usr/bin/dirname "${file_name}")
cover_file="${dir_name}/Front.jpg"
generic_icon_file="${HOME}/.local/share/icons/Dracula/apps/scalable/music_icon-24.svg"
history_file="${HOME}/.moc/history.csv"
history_playlist="${HOME}/Music/Albums/History.m3u"
message="$(echo -e "${artist}\n\n${track_name}")"

if [ -r "${cover_file}" ]; then
    icon_file="${cover_file}"
elif [ -d "${dir_name}" ]; then
    cd "${dir_name}" && cd ..

    if [ -r ./Front.jpg ]; then
        icon_file="${PWD}/Front.jpg"
    else
        icon_file="${generic_icon_file}"
    fi
else
    icon_file="${generic_icon_file}"
fi

/usr/bin/notify-send -i "${icon_file}" "${message}"
echo "${timestamp},${artist},${track_name},${file_name}" >> ${history_file}
echo "${file_name}" >> ${history_playlist}
