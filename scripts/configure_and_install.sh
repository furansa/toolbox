#!/usr/bin/env bash
#
# Automates system configuration and installation. Usage, as root:
#
# $ ./configure_and_install.sh [postman]
#
[ ${USER} = "root" ] || {
    echo "Error: You need to be root"
    exit 1
}

# Globals
DISTRO_NAME=$(lsb_release -i | awk '{print $3}')
DISTRO_CODENAME=$(lsb_release -cs)
KEYS_PATH="/etc/apt/trusted.gpg.d"
SOURCES_PATH="/etc/apt/sources.list.d"

function configure_environment {
    # TODO: Configure the apt repositories based on the distro name and codename
    echo -e "\nConfiguring the environment\n"

    apt-get update
}

function get_microsoft_key {
    [ -r ${KEYS_PATH}/microsoft.gpg ] || {
        wget -O- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /tmp/microsoft.gpg && \
        install -o root -g root -m 644 /tmp/microsoft.gpg ${KEYS_PATH}
    }

    return 0
}

function install_hardware_support {
    echo -e "\nInstalling hardware support packages\n"

    apt-get install acpi firmware-linux firmware-iwlwifi firmware-misc-nonfree \
    fprintd ifuse libimobiledevice6 libimobiledevice-utils lm-sensors lshw
}

function install_basic_tools {
    echo -e "\nInstalling very basic packages\n"

    apt-get install apt-transport-https bc cryptsetup gnupg libpam-fprintd mc \
    ncal pass pcscd rsync sudo tmux unrar unzip vim
}

function install_basic_desktop {
    echo -e "\nInstalling basic desktop packages\n"

    apt-get install blueman default-jre dunst fonts-fork-awesome \
    fonts-noto-core fonts-noto-mono fonts-noto-color-emoji fortunes-bofh-excuses \
    i3-wm i3lock i3status imagemagick libglib2.0-bin libnotify-bin network-manager-gnome \
    plymouth-themes pulsemixer pulseaudio pulseaudio-module-bluetooth qt5ct qutebrowser \
    redshift rofi rxvt-unicode update-desktop-database x11-xserver-utils \
    xinit xinput xloadimage xserver-xorg-video-intel
}

function install_general {
    echo -e "\nInstalling general software\n"

    # Brave
    # wget -O- https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg | gpg --dearmor > /tmp/brave-browser-keyring.gpg && \
    # install -o root -g root -m 644 /tmp/brave-browser-keyring.gpg ${KEYS_PATH}/ && \
    # echo "deb [arch=amd64 signed-by=${KEYS_PATH}/brave-browser-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main #Brave-Browser" > ${SOURCES_PATH}/brave-browser.list && \
    # apt-get update

    # Edge
    wget -O- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /tmp/packages.microsoft.gpg && \
    install -o root -g root -m 644 /tmp/packages.microsoft.gpg ${KEYS_PATH}/ && \
    echo "deb [arch=amd64,arm64,armhf signed-by=${KEYS_PATH}/packages.microsoft.gpg] https://packages.microsoft.com/repos/edge stable main #Edge" > ${SOURCES_PATH}/edge.list

    # Signal
    wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > /tmp/signal-desktop-keyring.gpg && \
    install -o root -g root -m 644 /tmp/signal-desktop-keyring.gpg ${KEYS_PATH}/ && \
    echo "deb [arch=amd64 signed-by=${KEYS_PATH}/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main #Signal" > ${SOURCES_PATH}/signal.list && \
    apt-get update

    # Skype
    wget -O- https://repo.skype.com/data/SKYPE-GPG-KEY | gpg --dearmor > /tmp/skype.gpg && \
    install -o root -g root -m 644 /tmp/skype.gpg ${KEYS_PATH}/ && \
    echo "deb [arch=amd64 signed-by=${KEYS_PATH}/skype.gpg] https://repo.skype.com/deb stable main #Skype" > ${SOURCES_PATH}/skype.list && \
    apt-get update

    # PicoScope
    # wget -O- https://labs.picotech.com/debian/dists/picoscope/Release.gpg.key | gpg --dearmor > /tmp/picoscope.gpg && \
    # install -o root -g root -m 644 /tmp/picoscope.gpg ${KEYS_PATH}/ && \
    # echo "deb [arch=amd64 signed-by=${KEYS_PATH}/picoscope.gpg] https://labs.picotech.com/debian/ picoscope main #Picoscope" > ${SOURCES_PATH}/picoscope.list && \
    # apt-get update

    COMMUNICATION="signal-desktop skypeforlinux"

    MULTIMEDIA="cdparanoia flac kid3-qt libheif-examples moc phototonic shotwell simplescreenrecorder sox vlc"
    # ffmpeg libdvdcss2 clementine ripperx musescore3 vlc-plugin-access-extra
    # audacity ardour calf-plugins cmt drumgizmo drumkv1-lv2 flowblade \
    # fluid-soundfont-g guitarix ir.lv2 playerctl qjackctl qtractor

    NETWORK="samba ufw uxplay"
    # iptraf nmap remmina-plugin-rdp tcpdump vinagre xtightvncviewer

    PRODUCTIVITY="calcurse hunspell-en-us hyphen-en-us libreoffice-calc libreoffice-draw \
    libreoffice-gtk3 libreoffice-writer pandoc texlive-latex-recommended zathura"
    # abook mutt newsboat w3m xournal texlive-latex-extra

    SCIENCE=""
    # boinc-manager kicad octave octave-signal stellarium

    for package in ${COMMUNICATION} ${MULTIMEDIA} ${NETWORK} ${PRODUCTIVITY} ${SCIENCE}; do
        apt-get install ${package}
    done
}

function install_dev_common {
    echo -e "\nInstalling development common software\n"

    # DBeaver CE
    wget -O- https://dbeaver.io/debs/dbeaver.gpg.key | gpg --dearmor > /tmp/dbeaver-ce.gpg && \
    install -o root -g root -m 644 /tmp/dbeaver-ce.gpg ${KEYS_PATH}/ && \
    echo "deb [arch=amd64 signed-by=${KEYS_PATH}/dbeaver-ce.gpg] https://dbeaver.io/debs/dbeaver-ce / #DBeaver-CE" > ${SOURCES_PATH}/dbeaver-ce.list && \

    # VSCode
    wget -O- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /tmp/packages.microsoft.gpg && \
    install -o root -g root -m 644 /tmp/packages.microsoft.gpg ${KEYS_PATH}/ && \
    echo "deb [arch=amd64,arm64,armhf signed-by=${KEYS_PATH}/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main #VSCode" > ${SOURCES_PATH}/vscode.list

    # Azure CLI
    echo "deb [arch=amd64 signed-by=${KEYS_PATH}/packages.microsoft.gpg] https://packages.microsoft.com/repos/azure-cli bullseye main #AzureCLI" > ${SOURCES_PATH}/azure-cli.list

    install_postman

    apt-get update && \
    apt-get install azure-cli=2.39.0-1~bullseye curl code dbeaver-ce git nodejs sen
}

function install_dev_cpp {
    echo -e "\nInstalling C and C++ development software\n"

    ANALYSIS="cppcheck"
    # linux-perf linux-tools-generic strace uftrace valgrind

    LIBS="libjpeg-dev libpng-dev libsdl2-dev libgl1-mesa-dev linux-headers-generic zlib1g-dev"

    LIBS_BOOST="libboost-all-dev"

    LIBS_JUCE="libgtk-3-dev libfreetype6-dev libxinerama-dev libxrandr-dev \
    libxcursor-dev mesa-common-dev libasound2-dev freeglut3-dev libxcomposite-dev \
    libcurl4-openssl-dev libwebkit2gtk-4.0-37 libwebkit2gtk-4.0-dev libx11-dev" 

    LIBS_PROTOBUF="libprotobuf-dev libprotoc17 protobuf-compiler"

    LIBS_REST="libcpprest-dev libcpprest-doc"

    LIBS_SFML="libsfml-dev libsfml-doc"

    TEST="catch google-mock googletest"

    TOOLS="build-essential ccache cmake gdb make"
    # autoconf chrpath clang clang-format cloc diffstat exuberant-ctags gawk texinfo"

    QT="qtcreator qtdeclarative5-dev"

    for package in ${ANALYSIS} ${TOOLS}; do
        apt-get install -y ${package}
    done
}

function install_dev_dotnet {
    echo -e "\nInstalling Dot Net development software\n"

    get_microsoft_key || exit 1

    echo "deb [arch=amd64,arm64,armhf signed-by=${KEYS_PATH}/microsoft.gpg] https://packages.microsoft.com/config/debian/12/prod.list bookworm main #DotNet" > ${SOURCES_PATH}/dotnet.list

    apt-get install dotnet-sdk-8.0
}

function install_dev_java {
    echo -e "\nInstalling Java development software\n"

    apt-get install default-jdk maven
}

function install_dev_go {
    echo -e "\nInstalling Go development software\n"

    apt-get install golang-go
}

function install_dev_python {
    echo -e "\nInstalling Python development software\n"

    apt-get install python-is-python3 flake8 pylint pipenv
}

function install_dev_orch_virt {
    echo -e "\nInstalling containerization, orchestration and virtualization software\n"

    # Docker CE
    wget -O- https://download.docker.com/linux/debian/gpg | gpg --dearmor > /tmp/docker.gpg && \
    install -o root -g root -m 644 /tmp/docker.gpg ${KEYS_PATH}/ && \
    echo "deb [arch=amd64 signed-by=${KEYS_PATH}/docker.gpg] https://download.docker.com/linux/debian bookworm stable #Docker" > ${SOURCES_PATH}/docker.list && \

    # VirtualBox
    wget -O- https://www.virtualbox.org/download/oracle_vbox_2016.asc | gpg --dearmor > /tmp/oracle_vbox_2016.gpg && \
    install -o root -g root -m 644 /tmp/oracle_vbox_2016.gpg ${KEYS_PATH}/ && \
    wget -O- https://www.virtualbox.org/download/oracle_vbox.asc | gpg --dearmor > /tmp/oracle_vbox.gpg && \
    install -o root -g root -m 644 /tmp/oracle_vbox.gpg ${KEYS_PATH}/ && \
    echo "deb [arch=amd64 signed-by=${KEYS_PATH}/oracle_vbox_2016.gpg] https://download.virtualbox.org/virtualbox/debian bullseye contrib #VirtualBox" > ${SOURCES_PATH}/virtualbox.list && \

    # Vagrant and Packer
    wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor > /tmp/hashicorp.gpg && \
    install -o root -g root -m 644 /tmp/hashicorp.gpg ${KEYS_PATH}/ && \
    echo "deb [arch=amd64 signed-by=${KEYS_PATH}/hashicorp.gpg] https://apt.releases.hashicorp.com bullseye main #Hashicorp" > ${SOURCES_PATH}/hashicorp.list && \

    # Kubectl
    wget -O- https://packages.cloud.google.com/apt/doc/apt-key.gpg | gpg --dearmor > /tmp/kubernetes.gpg && \
    install -o root -g root -m 644 /tmp/kubernetes.gpg ${KEYS_PATH}/ && \
    echo "deb [arch=amd64 signed-by=${KEYS_PATH}/kubernetes.gpg] https://apt.kubernetes.io kubernetes-xenial main #Kubernetes" > ${SOURCES_PATH}/kubernetes.list && \

    # Minikube
    wget -O /tmp/minikube_latest_amd64.deb https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb

    # OpenTofu
    wget -O- https://get.opentofu.org/opentofu.gpg | gpg --dearmor > /tmp/opentofu.gpg && \
    install -o root -g root -m 644 /tmp/opentofu.gpg ${KEYS_PATH}/ && \

    wget -O- https://packages.opentofu.org/opentofu/tofu/gpgkey | gpg --dearmor > /tmp/opentofu-repo.gpg && \
    install -o root -g root -m 644 /tmp/opentofu-repo.gpg ${KEYS_PATH}/ && \

    echo "deb [arch=amd64 signed-by=${KEYS_PATH}/opentofu.gpg,${KEYS_PATH}/opentofu-repo.gpg] https://packages.opentofu.org/opentofu/tofu/any/ any main #OpenTofu" > ${SOURCES_PATH}/opentofu.list

    apt-get update && \
    apt-get install docker-ce docker-compose kubectl linux-headers-amd64 vagrant virtualbox-6.1 packer tofu
    dpkg -i /tmp/minikube_latest_amd64.deb
}

function install_games {
    echo -e "\Installing games\n"

    dpkg --add-architecture i386

    apt-get install pcsxr pcsxr2 steam
}

function install_postman {
    echo -e "\nInstalling Postman\n"
    set -e

    DOWNLOAD_URL="https://dl.pstmn.io/download/latest/linux64"
    DOWNLOAD_FILE="/tmp/postman-latest.tar.gz"
    INSTALL_PATH="/opt/Postman"

    rm -Rf ${INSTALL_PATH} 2> /dev/null

    wget -O ${DOWNLOAD_FILE} ${DOWNLOAD_URL}

    tar pxzvf ${DOWNLOAD_FILE} -C /opt

    chown -R root:root ${INSTALL_PATH}

    chmod 0755 ${INSTALL_PATH}

    ln -sf ${INSTALL_PATH}/Postman /usr/local/bin/

    rm -Rf ${DOWNLOAD_FILE} 2> /dev/null
}

function remove_unwanted {
    echo -e "\nRemoving unwanted packages\n"

    apt-get autoremove --purge amd64-microcode installation-report modemmanager nano vim-tiny
}

function post_install {
    echo -e "\nPerforming post install tasks\n"

    apt-get clean -y
}

OPTION="${1}"

if [ "${OPTION}" = "postman" ]; then
    install_postman
else 
    configure_environment
    install_hardware_support
    install_basic_tools
    install_basic_desktop
    install_general
    install_dev_common
    install_dev_cpp
    install_dev_go
    install_dev_java
    install_dev_python
    install_dev_orch_virt
    remove_unwanted
    post_install
fi
