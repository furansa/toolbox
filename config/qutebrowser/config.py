#
# Qutebrowser configuration file, must be saved under config.py
#
# To set as default browser
# xdg-settings set default-web-browser org.qutebrowser.qutebrowser.desktop
# xdg-mime default org.qutebrowser.qutebrowser.desktop x-scheme-handler/http
# xdg-mime default org.qutebrowser.qutebrowser.desktop x-scheme-handler/https
#
import os

import themes.dracula.draw

themes.dracula.draw.blood(c, {
    "spacing": {
        "vertical": 1,
        "horizontal": 1
    }
})

QUTE_PROFILE = (
    os.getenv("QUTE_PROFILE") if os.getenv("QUTE_PROFILE") else "furansa"
)

if QUTE_PROFILE == "furansa":
    BG_COLOR_NORMAL = "#6272A4"
elif QUTE_PROFILE == "csw":
    BG_COLOR_NORMAL = "#BD93F9"

FG_COLOR_NORMAL = "#F8F8F2"
BG_COLOR_TAB_SELECTED = "#6272A4"
BG_COLOR_TAB_UNSELECTED = "#282A36"
BG_COLOR_PRIVATE = "#FF5555"
HOME = os.getenv("HOME")

config.load_autoconfig(False)

c.auto_save.session = True

c.colors.downloads.start.bg = "#50FA7B"
c.colors.statusbar.command.bg = BG_COLOR_NORMAL
c.colors.statusbar.command.fg = FG_COLOR_NORMAL
c.colors.statusbar.command.private.bg = BG_COLOR_PRIVATE
c.colors.statusbar.command.private.fg = FG_COLOR_NORMAL
c.colors.statusbar.insert.bg = BG_COLOR_NORMAL
c.colors.statusbar.normal.bg = BG_COLOR_NORMAL
c.colors.statusbar.private.bg = BG_COLOR_PRIVATE
c.colors.statusbar.url.fg = FG_COLOR_NORMAL
c.colors.statusbar.url.error.fg = "#FF5555"
c.colors.statusbar.url.success.http.fg = FG_COLOR_NORMAL
c.colors.statusbar.url.success.https.fg = FG_COLOR_NORMAL
c.colors.statusbar.url.warn.fg = "#F1FA8C"
c.colors.tabs.even.bg = BG_COLOR_TAB_UNSELECTED
c.colors.tabs.odd.bg = BG_COLOR_TAB_UNSELECTED
c.colors.tabs.pinned.even.bg = BG_COLOR_TAB_UNSELECTED
c.colors.tabs.pinned.odd.bg = BG_COLOR_TAB_UNSELECTED
c.colors.tabs.pinned.selected.even.bg = BG_COLOR_TAB_SELECTED
c.colors.tabs.pinned.selected.odd.bg = BG_COLOR_TAB_SELECTED
c.colors.tabs.selected.even.bg = BG_COLOR_TAB_SELECTED
c.colors.tabs.selected.odd.bg = BG_COLOR_TAB_SELECTED
c.colors.webpage.darkmode.enabled = True
c.colors.webpage.preferred_color_scheme = "dark"

c.completion.cmd_history_max_items = 50

c.content.autoplay = False
c.content.blocking.enabled = True
c.content.blocking.method = "both"  # pip install --user adblock
c.content.canvas_reading = True
c.content.cookies.accept = "no-3rdparty"
c.content.default_encoding = "utf-8"
c.content.geolocation = False
c.content.notifications.enabled = False
c.content.pdfjs = False
c.content.register_protocol_handler = False
# c.content.user_stylesheets = ["dracula.css"]
c.content.webgl = False

c.downloads.location.directory = f"{HOME}/Downloads"
c.downloads.location.remember = False
c.downloads.remove_finished = 60

c.fonts.default_family = ["Noto Sans Mono", "DejaVu Sans Mono"]
c.fonts.default_size = "11pt"
c.fonts.completion.category = "bold 11pt Noto Sans Mono"
c.fonts.completion.entry = "11pt Noto Sans Mono"
c.fonts.hints = c.fonts.completion.entry
c.fonts.messages.error = c.fonts.completion.entry
c.fonts.messages.info = c.fonts.completion.entry
c.fonts.messages.warning = c.fonts.completion.entry
c.fonts.prompts = c.fonts.completion.entry

c.session.lazy_restore = True

c.spellcheck.languages = ["en-US", "pt-BR", "pt-PT"]  # /usr/share/qutebrowser/scripts/dictcli.py install en-US

c.qt.args.append(f"widevine-path={HOME}/.config/qutebrowser/libwidevinecdm.so")

c.tabs.new_position.related = "last"

c.url.default_page = f"file:///{HOME}"

c.zoom.default = "125%"

config.bind("<Ctrl-B>", "quickmark-save", mode="normal")
config.bind("<Ctrl-R>", "restart", mode="normal")
config.bind("<Ctrl-Left>", "tab-prev", mode="normal")
config.bind("<Ctrl-Right>", "tab-next", mode="normal")
config.bind("<Ctrl-Shift-Left>", "tab-move -", mode="normal")
config.bind("<Ctrl-Shift-B>", "open -t qute://bookmarks", mode="normal")
config.bind("<Ctrl-Shift-O>", "set-cmd-text -s :open -p", mode="normal")
config.bind("<Ctrl-Shift-P>", "spawn --userscript qute-pass --unfiltered", mode="normal")
config.bind("<Ctrl-Shift-Right>", "tab-move +", mode="normal")

with config.pattern("https://*.proton.me") as p:
    p.content.cookies.accept = "all"
    p.content.notifications.enabled = True

with config.pattern("https://outlook.office.com") as p:
    p.content.cookies.accept = "all"
    p.content.notifications.enabled = True

with config.pattern("https://teams.microsoft.com") as p:
    p.content.cookies.accept = "all"
    p.content.desktop_capture = True
    p.content.media.audio_capture = True
    p.content.media.audio_video_capture = True
    p.content.media.video_capture = True
    p.content.notifications.enabled = True
